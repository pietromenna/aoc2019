package day06

import (
	"io/ioutil"
	"strings"
	"testing"
)

const sampleInput string = "COM)B\nB)C\nC)D\nD)E\nE)F\nB)G\nG)H\nD)I\nE)J\nJ)K\nK)L"
const sampleInput2 string = "COM)BB)C\nC)D\nD)E\nE)F\nB)G\nG)H\nD)I\nE)J\nJ)K\nK)L\nK)YOU\nI)SAN"

func readMyInput() string {
	dat, err := ioutil.ReadFile("input.txt")
	if err == nil {
		return string(dat)
	}
	return sampleInput
}

func Test_PartOneExample(t *testing.T) {
	expected := 42

	if actual := PartOne(sampleInput); actual != expected {
		t.Errorf("Expected: %d, actual: %d \n", expected, actual)
	}
}

func Test_PartOne(t *testing.T) {
	expected := 261306

	if actual := PartOne(readMyInput()); actual != expected {
		t.Errorf("Expected: %d, actual: %d \n", expected, actual)
	}
}

func indirectOrbitCount(g map[string]string, direct string, counter int) int {
	if el, ok := g[direct]; ok {
		return indirectOrbitCount(g, el, counter+1)
	} else {
		return counter
	}
}

func parseInput(s string) map[string]string {
	graph := make(map[string]string)

	lines := strings.Split(s, "\n")

	for _, line := range lines {
		objects := strings.Split(line, ")")
		graph[objects[1]] = objects[0]
	}

	return graph
}

func PartOne(s string) int {
	total := 0
	orbitCounts := make(map[string]int)
	graph := parseInput(s)

	for k, v := range graph {
		orbitCounts[k] = indirectOrbitCount(graph, v, 0)
	}

	for _, v := range orbitCounts {
		total = total + v
	}

	return total + len(graph)
}

func pathToRoot(g map[string]string, e string, l []string) []string {
	if el, ok := g[e]; ok {
		return pathToRoot(g, el, append(l, el))
	} else {
		return l
	}
}

func PartTwo(s string) int {
	graph := parseInput(s)

	//Get paths
	p1 := make([]string, 0)
	p2 := make([]string, 0)

	// Count distance to common and add
	p1 = pathToRoot(graph, graph["YOU"], p1)
	p2 = pathToRoot(graph, graph["SAN"], p2)

	//Find common distance
	for i, k := range p1 {
		for i2, k2 := range p2 {
			if k == k2 {
				return i + 2 + i2
			}
		}
	}

	return 0
}

func Test_PartTwoExample(t *testing.T) {
	expected := 4

	if actual := PartTwo(sampleInput2); actual != expected {
		t.Errorf("Expected: %d, actual: %d \n", expected, actual)
	}
}

func Test_PartTwo(t *testing.T) {
	expected := 382

	if actual := PartTwo(readMyInput()); actual != expected {
		t.Errorf("Expected: %d, actual: %d \n", expected, actual)
	}
}
