package day04

import (
	"strconv"
	"testing"
)

func PartOne(b, e int) int {
	count := 0
	for i := b; i <= e; i++ {
		if IsValid(strconv.Itoa(i)) {
			count += 1
		}
	}
	return count
}

func thereIsTwoAdjacentDigitsAreTheSame(s string) bool {
	chars := make(map[rune]int)
	for _, r := range s {
		if _, ok := chars[r]; ok {
			return true
		}
		chars[r] = 1
	}
	return false
}

func neverDecreesesFromLeftToRight(s string) bool {
	largest := 0
	for _, r := range s {
		if num, _ := strconv.Atoi(string(r)); num < largest {
			return false
		} else {
			largest = num
		}
	}
	return true
}

func IsValid(s string) bool {
	if len(s) != 6 || !thereIsTwoAdjacentDigitsAreTheSame(s) || !neverDecreesesFromLeftToRight(s) {
		return false
	}
	return true
}

func IsValid2(s string) bool {
	if len(s) != 6 || !neverDecreesesFromLeftToRight(s) || !thereIsOnlyTwoAdjacentDigitsAreTheSame(s) {
		return false
	}
	return true
}

func Test_PartOneExamples(t *testing.T) {
	tests := []struct {
		in     string
		result bool
	}{
		{"123789", false},
		{"1234567", false}, // More than 6 digits
		{"12345", false},   // Less than 6 digit
		{"111111", true},   // Never decreses
		{"223450", false},  //Decreses
	}

	for _, test := range tests {
		if IsValid(test.in) != test.result {
			t.Errorf("Failed for %s", test.in)
		}
	}
}

func Test_PartOne(t *testing.T) {
	expected := 1748
	if got := PartOne(146810, 612564); got != expected {
		t.Errorf("Got %d, expected %d", got, expected)
	}
}

func Test_PartTwoExamples(t *testing.T) {
	tests := []struct {
		in     string
		result bool
	}{
		{"112233", true},
		{"123444", false}, // no longer meets the criteria (the repeated 44 is part of a larger group of 444).
		{"111122", true},  //even though 1 is repeated more than twice, it still contains a double 22
		{"222244", true},
		{"114444", true},
		{"113344", true},
	}

	for _, test := range tests {
		if IsValid2(test.in) != test.result {
			t.Errorf("Failed for %s", test.in)
		}
	}
}

func thereIsOnlyTwoAdjacentDigitsAreTheSame(s string) bool {
	chars := make(map[rune]int)
	for _, r := range s {
		if _, ok := chars[r]; !ok {
			chars[r] = 1
		} else {
			chars[r] = chars[r] + 1
		}
	}

	for _, v := range chars {
		if v == 2 {
			return true
		}
	}
	return false
}

func Test_PartTwo(t *testing.T) {
	expected := 1180
	if got := PartTwo(146810, 612564); got != expected {
		t.Errorf("Got %d, expected %d", got, expected)
	}
}

func PartTwo(b, e int) int {
	count := 0
	for i := b; i <= e; i++ {
		if IsValid2(strconv.Itoa(i)) {
			count += 1
		}
	}
	return count
}
