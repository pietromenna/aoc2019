package day02

import (
	"fmt"
	"strconv"
	"strings"
	"testing"
)

func process(m []int) []int {
	pointer := 0
	running := 1
	for running == 1 {
		if m[pointer] == 1 { // add
			a := m[m[pointer+1]]
			b := m[m[pointer+2]]
			m[m[pointer+3]] = a + b
			pointer += 4
		} else if m[pointer] == 2 { // multiply
			a := m[m[pointer+1]]
			b := m[m[pointer+2]]
			m[m[pointer+3]] = a * b
			pointer += 4
		} else if m[pointer] == 99 {
			running = 0
		} else {
			fmt.Print("Whoops")
		}
	}
	return m
}

func PartOne(i string) int {
	memory := stringToMemory(i)

	memory = process(memory)

	return memory[0]
}

func stringToMemory(i string) []int {
	s := strings.Split(i, ",")
	memory := make([]int, len(s))
	for i, number := range s {
		memory[i], _ = strconv.Atoi(number)
	}
	return memory
}

func TestPartOne(t *testing.T) {
	input := "1,12,2,3,1,1,2,3,1,3,4,3,1,5,0,3,2,6,1,19,1,5,19,23,2,6,23,27,1,27,5,31,2,9,31,35,1,5,35,39,2,6,39,43,2,6,43,47,1,5,47,51,2,9,51,55,1,5,55,59,1,10,59,63,1,63,6,67,1,9,67,71,1,71,6,75,1,75,13,79,2,79,13,83,2,9,83,87,1,87,5,91,1,9,91,95,2,10,95,99,1,5,99,103,1,103,9,107,1,13,107,111,2,111,10,115,1,115,5,119,2,13,119,123,1,9,123,127,1,5,127,131,2,131,6,135,1,135,5,139,1,139,6,143,1,143,6,147,1,2,147,151,1,151,5,0,99,2,14,0,0"

	out := PartOne(input)
	if out != 30 {
		t.Errorf("%d is not 30", out)
	}
}

func TestPartTwo(t *testing.T) {
	input := "1,0,0,3,1,1,2,3,1,3,4,3,1,5,0,3,2,6,1,19,1,5,19,23,2,6,23,27,1,27,5,31,2,9,31,35,1,5,35,39,2,6,39,43,2,6,43,47,1,5,47,51,2,9,51,55,1,5,55,59,1,10,59,63,1,63,6,67,1,9,67,71,1,71,6,75,1,75,13,79,2,79,13,83,2,9,83,87,1,87,5,91,1,9,91,95,2,10,95,99,1,5,99,103,1,103,9,107,1,13,107,111,2,111,10,115,1,115,5,119,2,13,119,123,1,9,123,127,1,5,127,131,2,131,6,135,1,135,5,139,1,139,6,143,1,143,6,147,1,2,147,151,1,151,5,0,99,2,14,0,0"
	initialMemory := stringToMemory(input)
	noun := 0
	verb := 0
	out := 0
	for noun = 0; noun < 100; noun++ {
		for verb = 0; verb < 100; verb++ {
			memory := make([]int, len(initialMemory))
			n := copy(memory, initialMemory)
			if n > 0 {
				memory[1] = noun
				memory[2] = verb
				m := process(memory)
				out = m[0]
			}

			if out == 19690720 {
				fmt.Printf("noun %d verb %d \n", noun, verb)
				fmt.Printf("use: %d \n", (100*noun)+verb)
				t.Fail()
			}
		}
	}

	t.Fail()
}
