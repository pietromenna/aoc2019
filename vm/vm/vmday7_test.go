package vm

import "testing"

func TestDay7ExamplesPartOne(t *testing.T) {
	testCases := []struct {
		memory string
		phase  []int
		signal int
	}{
		{"3,15,3,16,1002,16,10,16,1,16,15,15,4,15,99,0,0", []int{4, 3, 2, 1, 0}, 43210},
		{"3,23,3,24,1002,24,10,24,1002,23,-1,23,101,5,23,23,1,24,23,23,4,23,99,0,0", []int{0, 1, 2, 3, 4}, 54321},
		{"3,31,3,32,1002,32,10,32,1001,31,-2,31,1007,31,0,33,1002,33,7,33,1,33,31,31,1,32,31,31,4,31,99,0,0,0", []int{1, 0, 4, 3, 2}, 65210},
	}

	for _, tc := range testCases {
		if out := Day7SettingPartOne(tc.memory, tc.phase); out != tc.signal {
			t.Errorf("Expected %d, got %d ", tc.signal, out)
		}
	}
}

func TestDay7PartOne(t *testing.T) {
	perms := permutations([]int{0, 1, 2, 3, 4})

	max := 0

	for _, p := range perms {
		if signal := Day7SettingPartOne(day7Input, p); signal > max {
			max = signal
		}
	}

	if max != 366376 {
		t.Errorf("%d is not 366376", max)
	}
}

func Day7SettingPartOne(instructions string, phase []int) int {
	memory := StringToMemory(instructions)

	inA := make(chan int, 1)
	inB := make(chan int, 1)
	inC := make(chan int, 1)
	inD := make(chan int, 1)
	inE := make(chan int, 1)
	thrusterIn := make(chan int, 1)

	a := IntCodeComputer7{Memory: memory, Name: "A", In: inA, Out: inB, Phase: phase[0], phaseConsumed: false}
	b := IntCodeComputer7{Memory: memory, Name: "B", In: inB, Out: inC, Phase: phase[1], phaseConsumed: false}
	c := IntCodeComputer7{Memory: memory, Name: "C", In: inC, Out: inD, Phase: phase[2], phaseConsumed: false}
	d := IntCodeComputer7{Memory: memory, Name: "D", In: inD, Out: inE, Phase: phase[3], phaseConsumed: false}
	e := IntCodeComputer7{Memory: memory, Name: "E", In: inE, Out: thrusterIn, Phase: phase[4], phaseConsumed: false}

	inA <- 0

	a.Process()
	b.Process()
	c.Process()
	d.Process()
	e.Process()
	return <-thrusterIn
}

//func Day7SettingPartTwo(instructions string, phase []int) int {
//	memory := StringToMemory(instructions)
//
//	inA := make(chan int,1)
//	inB := make(chan int,1)
//	inC := make(chan int,1)
//	inD := make(chan int,1)
//	inE := make(chan int,1)
//	thrusterOut := make(chan int,1)
//
//	a := IntCodeComputer7{Memory:memory, Name: "A", In: inA, Out: inB, Phase: phase[0], phaseConsumed: false}
//	b := IntCodeComputer7{Memory:memory, Name: "B", In: inB, Out: inC, Phase: phase[1], phaseConsumed: false }
//	c := IntCodeComputer7{Memory:memory, Name: "C", In: inC, Out: inD, Phase: phase[2], phaseConsumed: false }
//	d := IntCodeComputer7{Memory:memory, Name: "D", In: inD, Out: inE, Phase: phase[3], phaseConsumed: false }
//	e := IntCodeComputer7{Memory:memory, Name: "E", In: inE, Out: inA, Phase: phase[4], phaseConsumed: false }
//
//	inA <- 0
//
//	a.Process()
//	b.Process()
//	c.Process()
//	d.Process()
//	e.Process()
//
//	return <- thrusterOut
//}
//
//func TestDay7ExamplesPartTwo(t *testing.T) {
//	testCases := []struct{
//		memory string
//		phase []int
//		signal int
//	}{
//		{"3,26,1001,26,-4,26,3,27,1002,27,2,27,1,27,26,27,4,27,1001,28,-1,28,1005,28,6,99,0,0,5", []int{9,8,7,6,5}, 139629729,},
//		{"3,52,1001,52,-5,52,3,53,1,52,56,54,1007,54,5,55,1005,55,26,1001,54,-5,54,1105,1,12,1,53,54,53,1008,54,0,55,1001,55,1,55,2,53,55,53,4,53,1001,56,-1,56,1005,56,6,99,0,0,0,0,10", []int{9,7,8,5,6}, 18216,},
//	}
//
//	for _, tc := range testCases {
//		if out := Day7SettingPartTwo(tc.memory, tc.phase); out != tc.signal {
//			t.Errorf("Expected %d, got %d ", tc.signal, out)
//		}
//	}
//}
