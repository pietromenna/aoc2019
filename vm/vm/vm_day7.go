package vm

import "fmt"

type IntCodeComputer7 struct {
	Name          string
	Memory        []int
	In            chan int
	Out           chan int
	Phase         int
	phaseConsumed bool
	pointer       int
	status        int
}

func (v *IntCodeComputer7) getParams(opCode OpCode) (int, int) {
	var a int
	var b int
	if opCode.ParameterMode[0] == 0 {
		a = v.Memory[v.Memory[v.pointer+1]]
	} else {
		a = v.Memory[v.pointer+1]
	}
	if opCode.ParameterMode[1] == 0 {
		b = v.Memory[v.Memory[v.pointer+2]]
	} else {
		b = v.Memory[v.pointer+2]
	}
	return a, b
}

func (v *IntCodeComputer7) Process() []int {
	v.pointer = 0
	v.status = 1
	for v.status == 1 {
		opCode := Fetch(v.Memory[v.pointer])
		if opCode.Op == 1 { // add
			a, b := v.getParams(opCode)
			v.Memory[v.Memory[v.pointer+3]] = a + b
			v.pointer += 4
		} else if opCode.Op == 2 { // multiply
			a, b := v.getParams(opCode)
			v.Memory[v.Memory[v.pointer+3]] = a * b
			v.pointer += 4
		} else if opCode.Op == 3 { // read
			var input int
			if v.phaseConsumed {
				input = <-v.In
			} else {
				input = v.Phase
				v.phaseConsumed = true
			}
			addressToWrite := v.Memory[v.pointer+1]
			v.Memory[addressToWrite] = input
			v.pointer += 2
		} else if opCode.Op == 4 { // output
			var addressToRead int
			if opCode.ParameterMode[0] == 1 {
				addressToRead = v.pointer + 1
			} else {
				addressToRead = v.Memory[v.pointer+1]
			}
			v.Out <- v.Memory[addressToRead]
			v.pointer += 2
		} else if opCode.Op == 5 { // jump-if-true
			a, b := v.getParams(opCode)
			if a != 0 {
				v.pointer = b
			} else {
				v.pointer += 3
			}
		} else if opCode.Op == 6 { // jump-if-false
			a, b := v.getParams(opCode)
			if a == 0 {
				v.pointer = b
			} else {
				v.pointer += 3
			}
		} else if opCode.Op == 7 { // less than
			a, b := v.getParams(opCode)
			if a < b {
				v.Memory[v.Memory[v.pointer+3]] = 1
			} else {
				v.Memory[v.Memory[v.pointer+3]] = 0
			}
			v.pointer += 4
		} else if opCode.Op == 8 { // equals
			a, b := v.getParams(opCode)
			if a == b {
				v.Memory[v.Memory[v.pointer+3]] = 1
			} else {
				v.Memory[v.Memory[v.pointer+3]] = 0
			}
			v.pointer += 4
		} else if opCode.Op == 99 {
			v.status = 0
		} else {
			fmt.Printf("Whoops \n Memory: %d \n IP: %d \n IT %d\n", v.Memory, v.pointer, opCode.Op)
			v.status = 0
		}
	}
	return v.Memory
}

func permutations(arr []int) [][]int {
	var helper func([]int, int)
	res := [][]int{}

	helper = func(arr []int, n int) {
		if n == 1 {
			tmp := make([]int, len(arr))
			copy(tmp, arr)
			res = append(res, tmp)
		} else {
			for i := 0; i < n; i++ {
				helper(arr, n-1)
				if n%2 == 1 {
					tmp := arr[i]
					arr[i] = arr[n-1]
					arr[n-1] = tmp
				} else {
					tmp := arr[0]
					arr[0] = arr[n-1]
					arr[n-1] = tmp
				}
			}
		}
	}
	helper(arr, len(arr))
	return res
}
