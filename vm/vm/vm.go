package vm

import (
	"fmt"
	"strconv"
	"strings"
)

type IntCodeComputer struct {
	Memory       []int
	pointer      int
	status       int
	relativeBase int
	output       string
}

type OpCode struct {
	Op            int
	ParameterMode []int
}

func (v *IntCodeComputer) getParams(opCode OpCode) (int, int, int) {
	var a, b, c int

	if opCode.ParameterMode[0] == 0 { // position
		a = v.Memory[v.Memory[v.pointer+1]]
	} else if opCode.ParameterMode[0] == 1 { //immediate
		a = v.Memory[v.pointer+1]
	} else if opCode.ParameterMode[0] == 2 { // relative
		a = v.Memory[v.Memory[v.pointer+1]+v.relativeBase]
	}

	if opCode.ParameterMode[1] == 0 {
		b = v.Memory[v.Memory[v.pointer+2]]
	} else if opCode.ParameterMode[1] == 1 {
		b = v.Memory[v.pointer+2]
	} else if opCode.ParameterMode[1] == 2 {
		b = v.Memory[v.Memory[v.pointer+2]+v.relativeBase]
	}

	if opCode.ParameterMode[2] == 2 {
		c = v.Memory[v.pointer+3] + v.relativeBase
	} else {
		c = v.Memory[v.pointer+3]
	}

	return a, b, c
}

func StringToMemory(i string) []int {
	s := strings.Split(i, ",")
	memory := make([]int, len(s))
	for i, number := range s {
		memory[i], _ = strconv.Atoi(number)
	}
	return memory
}

func Fetch(a int) OpCode {
	var parameterMode []int
	var i int
	s := strconv.Itoa(a)
	if len(s) < 3 {
		return OpCode{a, []int{0, 0, 0}}
	} else {
		for len(s) < 5 {
			s = "0" + s
		}
		p1, _ := strconv.Atoi(s[2:3])
		p2, _ := strconv.Atoi(s[1:2])
		p3, _ := strconv.Atoi(s[0:1])
		parameterMode = []int{p1, p2, p3}
	}
	if s[3] == 0 {
		i = int(s[5])
	} else {
		i, _ = strconv.Atoi(s[4:])
	}
	return OpCode{i, parameterMode}
}

func (v *IntCodeComputer) GetPrint() string {
	return v.output
}

func (v *IntCodeComputer) Process() []int {
	v.pointer = 0
	v.status = 1
	v.relativeBase = 0
	for v.status == 1 {
		opCode := Fetch(v.Memory[v.pointer])
		if opCode.Op == 1 { // add
			a, b, c := v.getParams(opCode)
			v.Memory[c] = a + b
			v.pointer += 4
		} else if opCode.Op == 2 { // multiply
			a, b, c := v.getParams(opCode)
			v.Memory[c] = a * b
			v.pointer += 4
		} else if opCode.Op == 3 { // read
			var input, addressToWrite int
			fmt.Scanf("%d\n", &input)
			addressToWrite = v.getAddressTo(opCode)
			v.Memory[addressToWrite] = input
			v.pointer += 2
		} else if opCode.Op == 4 { // output
			addressToRead := v.getAddressTo(opCode)
			v.output = fmt.Sprintf("%s%d\n", v.output, v.Memory[addressToRead])
			v.pointer += 2
		} else if opCode.Op == 5 { // jump-if-true
			a, b, _ := v.getParams(opCode)
			if a != 0 {
				v.pointer = b
			} else {
				v.pointer += 3
			}
		} else if opCode.Op == 6 { // jump-if-false
			a, b, _ := v.getParams(opCode)
			if a == 0 {
				v.pointer = b
			} else {
				v.pointer += 3
			}
		} else if opCode.Op == 7 { // less than
			a, b, c := v.getParams(opCode)
			if a < b {
				v.Memory[c] = 1
			} else {
				v.Memory[c] = 0
			}
			v.pointer += 4
		} else if opCode.Op == 8 { // equals
			a, b, c := v.getParams(opCode)
			if a == b {
				v.Memory[c] = 1
			} else {
				v.Memory[c] = 0
			}
			v.pointer += 4
		} else if opCode.Op == 9 { // adjust relative base
			addressToRead := v.getAddressTo(opCode)
			v.relativeBase = v.relativeBase + v.Memory[addressToRead]
			v.pointer += 2
		} else if opCode.Op == 99 {
			v.status = 0
		} else {
			fmt.Printf("Whoops \n Memory: %d \n IP: %d \n IT %d\n", v.Memory, v.pointer, opCode.Op)
			v.status = 0
		}
	}
	return v.Memory
}

func (v *IntCodeComputer) getAddressTo(opCode OpCode) int {
	var address int
	if opCode.ParameterMode[0] == 1 {
		address = v.pointer + 1
	} else if opCode.ParameterMode[0] == 0 {
		address = v.Memory[v.pointer+1]
	} else if opCode.ParameterMode[0] == 2 {
		address = v.Memory[v.pointer+1] + v.relativeBase
	}
	return address
}
