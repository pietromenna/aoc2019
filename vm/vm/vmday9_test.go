package vm

import (
	"testing"
)

func Test_Day9PartOneExamples(t *testing.T) {
	testCases := []struct {
		code           string
		expectedOutput string
	}{
		{"109,1,204,-1,1001,100,1,100,1008,100,16,101,1006,101,0,99","109\n1\n204\n-1\n1001\n100\n1\n100\n1008\n100\n16\n101\n1006\n101\n0\n99\n"},
		{"1102,34915192,34915192,7,4,7,99,0", "1219070632396864\n"},
		{"104,1125899906842624,99", "1125899906842624\n"},
		{"109,-1,4,1,99","-1\n"},
		{"109,-1,104,1,99","1\n"},
		{"109,-1,204,1,99","109\n"},
		{"109,1,9,2,204,-6,99","204\n"},
		{"109,1,109,9,204,-6,99","204\n"},
		{"109,1,209,-1,204,-106,99","204\n"},
		//{"109,1,3,3,204,2,99","1\n"},
		//{"109,1,203,2,204,2,99","1\n"},

	}

	for _, tc := range testCases {
		if result := Day9Run(tc.code); result != tc.expectedOutput {
			t.Errorf("Expected %s, got: %s", tc.expectedOutput, result)
		}
	}
}

func Day9Run(code string) string {
	var extraMemory []int
	initialMemory := StringToMemory(code)
	for i := 0; i < 3000; i++ {
		extraMemory = append(extraMemory, 0)
	}
	initialMemory = append(initialMemory, extraMemory...)
	m := IntCodeComputer{Memory: initialMemory}
	m.Process()

	return m.GetPrint()
}
