package vm

import (
	"testing"
)

const day7Input string = "3,8,1001,8,10,8,105,1,0,0,21,38,47,64,85,106,187,268,349,430,99999,3,9,1002,9,4,9,1001,9,4,9,1002,9,4,9,4,9,99,3,9,1002,9,4,9,4,9,99,3,9,1001,9,3,9,102,5,9,9,1001,9,5,9,4,9,99,3,9,101,3,9,9,102,5,9,9,1001,9,4,9,102,4,9,9,4,9,99,3,9,1002,9,3,9,101,2,9,9,102,4,9,9,101,2,9,9,4,9,99,3,9,1002,9,2,9,4,9,3,9,102,2,9,9,4,9,3,9,1001,9,2,9,4,9,3,9,1001,9,1,9,4,9,3,9,101,1,9,9,4,9,3,9,102,2,9,9,4,9,3,9,101,2,9,9,4,9,3,9,102,2,9,9,4,9,3,9,1002,9,2,9,4,9,3,9,1002,9,2,9,4,9,99,3,9,102,2,9,9,4,9,3,9,1002,9,2,9,4,9,3,9,1001,9,1,9,4,9,3,9,1002,9,2,9,4,9,3,9,101,1,9,9,4,9,3,9,1002,9,2,9,4,9,3,9,1001,9,2,9,4,9,3,9,1001,9,1,9,4,9,3,9,101,2,9,9,4,9,3,9,101,1,9,9,4,9,99,3,9,102,2,9,9,4,9,3,9,102,2,9,9,4,9,3,9,1001,9,1,9,4,9,3,9,1002,9,2,9,4,9,3,9,102,2,9,9,4,9,3,9,1002,9,2,9,4,9,3,9,101,1,9,9,4,9,3,9,101,1,9,9,4,9,3,9,101,1,9,9,4,9,3,9,1002,9,2,9,4,9,99,3,9,1002,9,2,9,4,9,3,9,102,2,9,9,4,9,3,9,101,1,9,9,4,9,3,9,1001,9,1,9,4,9,3,9,1002,9,2,9,4,9,3,9,102,2,9,9,4,9,3,9,102,2,9,9,4,9,3,9,101,2,9,9,4,9,3,9,102,2,9,9,4,9,3,9,1002,9,2,9,4,9,99,3,9,1002,9,2,9,4,9,3,9,101,1,9,9,4,9,3,9,102,2,9,9,4,9,3,9,1001,9,2,9,4,9,3,9,1002,9,2,9,4,9,3,9,1002,9,2,9,4,9,3,9,1001,9,1,9,4,9,3,9,1002,9,2,9,4,9,3,9,1001,9,1,9,4,9,3,9,102,2,9,9,4,9,99"

func Day2PartOne(i string) int {
	memory := StringToMemory(i)

	v := IntCodeComputer{Memory: memory}
	memory = v.Process()

	return memory[0]
}

func TestDay2PartOne(t *testing.T) {
	input := "1,12,2,3,1,1,2,3,1,3,4,3,1,5,0,3,2,6,1,19,1,5,19,23,2,6,23,27,1,27,5,31,2,9,31,35,1,5,35,39,2,6,39,43,2,6,43,47,1,5,47,51,2,9,51,55,1,5,55,59,1,10,59,63,1,63,6,67,1,9,67,71,1,71,6,75,1,75,13,79,2,79,13,83,2,9,83,87,1,87,5,91,1,9,91,95,2,10,95,99,1,5,99,103,1,103,9,107,1,13,107,111,2,111,10,115,1,115,5,119,2,13,119,123,1,9,123,127,1,5,127,131,2,131,6,135,1,135,5,139,1,139,6,143,1,143,6,147,1,2,147,151,1,151,5,0,99,2,14,0,0"

	out := Day2PartOne(input)
	if out != 4484226 {
		t.Errorf("%d is not 4484226", out)
	}
}

func TestDay2PartTwo(t *testing.T) {
	input := "1,0,0,3,1,1,2,3,1,3,4,3,1,5,0,3,2,6,1,19,1,5,19,23,2,6,23,27,1,27,5,31,2,9,31,35,1,5,35,39,2,6,39,43,2,6,43,47,1,5,47,51,2,9,51,55,1,5,55,59,1,10,59,63,1,63,6,67,1,9,67,71,1,71,6,75,1,75,13,79,2,79,13,83,2,9,83,87,1,87,5,91,1,9,91,95,2,10,95,99,1,5,99,103,1,103,9,107,1,13,107,111,2,111,10,115,1,115,5,119,2,13,119,123,1,9,123,127,1,5,127,131,2,131,6,135,1,135,5,139,1,139,6,143,1,143,6,147,1,2,147,151,1,151,5,0,99,2,14,0,0"
	initialMemory := StringToMemory(input)
	noun := 0
	verb := 0
	out := 0
	expectedNoun := 56
	expectedVerb := 96
	for noun = 0; noun < 100; noun++ {
		for verb = 0; verb < 100; verb++ {
			memory := make([]int, len(initialMemory))
			n := copy(memory, initialMemory)
			if n > 0 {
				memory[1] = noun
				memory[2] = verb
				v := IntCodeComputer{Memory: memory}
				m := v.Process()
				out = m[0]
			}

			if out == 19690720 {
				if noun != expectedNoun || verb != expectedVerb {
					t.Errorf("got verb %d, exptected: %d. Got noun %d, expected: %d", verb, expectedVerb, noun, expectedNoun)
				}
				//fmt.Printf("use: %d \n", (100*noun)+verb)
			}
		}
	}
}

func TestFetchInstruction(t *testing.T) {
	tests := []struct {
		input  int
		opCode OpCode
	}{
		{1, OpCode{1, []int{0, 0, 0}}},
		{2, OpCode{2, []int{0, 0, 0}}},
		{1002, OpCode{2, []int{0, 1, 0}}},
		{0002, OpCode{2, []int{0, 0, 0}}},
		{1102, OpCode{2, []int{1, 1, 0}}},
		{102, OpCode{2, []int{1, 0, 0}}},
	}

	for _, tc := range tests {
		if oc := Fetch(tc.input); oc.Op != tc.opCode.Op {
			t.Errorf("Expected %d Got %d in %d (%d)", tc.opCode.Op, oc.Op, tc.input, tc)
		} else {
			for i, p := range oc.ParameterMode {
				if p != tc.opCode.ParameterMode[i] {
					t.Errorf("Expected %d, got %d in %d", tc.opCode.ParameterMode, oc.ParameterMode, tc.input)
				}
			}
		}
	}
}

func TestDay5Example(t *testing.T) {
	sampleProgram := "1002,4,3,4,33"

	memory := StringToMemory(sampleProgram)

	v := IntCodeComputer{Memory: memory}
	memory = v.Process()

	expected := 99
	if memory[4] != 99 {
		t.Errorf("expected %d got %d", expected, memory[4])
	}
}

func TestDay5ExampleIo(t *testing.T) {
	sampleProgram := "3,0,4,0,99"

	memory := StringToMemory(sampleProgram)

	v := IntCodeComputer{Memory: memory}
	memory = v.Process()

	expected := 99
	if memory[4] != 99 {
		t.Errorf("expected %d got %d", expected, memory[4])
	}
}
