def fuel_for_module(mod)
	return ((mod / 3).floor() - 2)
end

def part1
	file_data = File.read("input.txt").split
	s = 0
	file_data.each do |m|
		s += fuel_for_module(m.to_i)
	end
	return s
end

#puts "part one: " + part1

def fuel_for_fuel(f)
	all_fuel = 0
	fuel = f
	while fuel > 0 do
		fuel = ((fuel / 3).floor() - 2)
		if fuel < 0
			fuel = 0
		end
		all_fuel += fuel		
	end

	return all_fuel
end

def fuel_for_module_with_fuel(mod)
	ffm = fuel_for_module(mod)
	fff = fuel_for_fuel(ffm)
	return ffm + fff
end

def part2
	file_data = File.read("input.txt").split
	s = 0
	file_data.each do |m|
		s += fuel_for_module_with_fuel(m.to_i)
	end
	return s
end

puts "part two: " + part2.to_s